<?php include("includes/header.php"); ?>
<?php if (!$session->is_signed_in()){ redirect("login.php"); } ?>

<?php 

if(empty($_GET['id'])){
    redirect("photos.php");
} else{

$categories = Category::find_all();
$category = new Category;

$photo = Photo::find_by_id($_GET['id']);

if (isset($_POST['update'])) 
{
    if ($photo) {

        $photo->id               = $_GET['id'];    
        $photo->title            = $_POST['title'];
        $photo->caption          = $_POST['caption'];
        $photo->alternative_text = $_POST['alternative_text'];
        $photo->description      = $_POST['description'];
        $photo->category_id      = $_POST['cat_id'];

        $photo->save();
    }
}


   
}
//$photos = Photo::find_all();

?>

    <!-- Navigation -->

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

    <!-- Brand and toggle get grouped for better mobile display -->

    <?php include("includes/top_nav.php");  ?>

    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

    <?php include("includes/side_nav.php"); ?>
    
    <!-- /.navbar-collapse -->

</nav>

<div id="page-wrapper">

	<div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">

                <h1 class="page-header">Edit Photo<small> | In progress</small></h1>

                <form action="" method="post">
               		<div class="col-md-8">

                        <div class="form-group">
                            <input type="text" name="title" class="form-control" label="" value="<?php echo $photo->title; ?>">
                        </div>

                        <div class="form-group">
                            <a class="thumbnail" href="#"><img src="<?php echo $photo->picture_path(); ?>" alt=""></a>
                        </div>

                        <div class="form-group">
                            <label for="caption">Caption</label>
                            <input type="text" name="caption" class="form-control" label="" value="<?php echo $photo->caption; ?>">
                        </div>

                        <div class="form-group">
                            <label for="category">Category</label>
                            <select class="form-control" name="cat_id">                                
                               
                                <option value="">
                                    <?php 
                                    if (!isset($_POST['cat_id'])) {
                                        $cat = $category->find_category($_GET['catid']);
                                    } else {
                                        $cat = $category->find_category($photo->category_id);
                                    }
                                    ?>
                                </option>  
                                                              
                                <?php foreach ($categories as $category) : ?>                                    
                                    <option value="<?php echo $category->id; ?>"><?php echo $category->cat_title; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="alternative">Alternative text</label>
                            <input type="text" name="alternative_text" class="form-control" label="" value="<?php echo $photo->alternative_text; ?>">
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea  class="form-control" id="editor" name="description" col="30" rows="10" ><?php echo $photo->description; ?></textarea>
                            
                        </div>
                    </div><!-- /.col-md-8 -->

                    <!-- -------------------------------------------------------------------------------------->

                    <div class="col-md-4" >
       
                        <div  class="photo-info-box">                 
                            <div class="info-box-header">
                                <h4>Save <span id="toggle" class="glyphicon glyphicon-menu-up pull-right"></span></h4>
                            </div>
                      
                            <div class="inside">
                                <div class="box-inner">
                                    <p class="text">
                                        <span class="glyphicon glyphicon-calendar"></span>Uploaded on: April 22, 2030 @ 5:26
                                    </p>
                                    
                                    <p class="text ">
                                        Photo Id: <span class="data photo_id_box"><?php echo $photo->id; ?></span>
                                    </p>
                                    
                                    <p class="text">
                                        Filename: <span class="data">"<?php echo $photo->filename; ?>"</span>
                                    </p>
                                    
                                    <p class="text">
                                        File Type: <span class="data">"<?php echo $photo->type; ?>"</span>
                                    </p>
                                    
                                    <p class="text">
                                        File Size: <span class="data">"<?php echo $photo->size; ?>"</span>
                                    </p>
                                  
                                </div><!-- box-inner -->

                                <div class="info-box-footer clearfix">
                                    <div class="info-box-delete pull-left">
                                        <a  href="delete_photo.php?id=<?php echo $photo->id; ?>" class="btn btn-danger btn-md ">Delete</a>   
                                    </div>
                              
                                    <div class="info-box-update pull-right ">
                                        <input type="submit" name="update" value="Update" class="btn btn-primary btn-md ">
                                    </div>   
                                </div><!-- info box footer-->
                            </div><!-- inside -->
                        </div><!-- Photo info box -->
                    </div><!-- /.col-md-4 -->
                </form><!-- /.Form-->

                <!-- ------------------------------------------------------------------------------------ -->                 

            </div><!-- /.col-md-12 -->
    	</div> <!-- /.row -->
	</div><!-- /.container-fluid -->              
</div> <!-- /#page-wrapper -->
            


   

<?php include("includes/footer.php"); ?>