<?php include("includes/header.php");

if (!$session->is_signed_in()) { 

    redirect("login.php");
}

//$categories = new Category;
$category = Category::find_by_id($_GET['id']);



if (isset($_POST['update'])) {
    
    if ($category) {
        
        $category->cat_id    = $_GET['id'];
        $category->cat_title = $_POST['title'];

        $category->save();
        
        redirect("categories.php");
        $session->message("Category {$category->cat_title} Updated!");
    }
}



?>

<!-- Navigation -->    
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

    
    <!-- Top Nav -->
    <?php include("includes/top_nav.php");  ?>

    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <?php include("includes/side_nav.php"); ?>
 
</nav><!-- / navigation -->

<div id="page-wrapper">

	<div class="container-fluid">
        <div class="row">

            <!-- Page Heading -->
            <h1 class="page-header">Categories<small> | Work in Progress</small></h1>
            
            <div class="col-lg-12">              
                

                <!-- Category Entry Form -->
                <form action="" method="post">
                    <div class="form-group">
                        <label for="title">New Category Name</label>
                        <input value="<?php if (isset($category->cat_title)){ echo $category->cat_title;} ?>" class="form-control" type="text" name="title">
                    </div>

                    <div class="form-group">
                        <input class="btn btn-primary" type="submit" name="update" value="Update">
                    </div>
                </form>
                   
            </div><!-- /.col-lg-12 -->
    	</div> <!-- /.row -->
	</div><!-- /.container-fluid -->              
</div> <!-- /#page-wrapper -->
               

<?php include("includes/footer.php"); ?>