<?php include("includes/header.php"); ?>
<?php if (!$session->is_signed_in()){ redirect("login.php"); } ?>

<?php 

$user   = new User;
$photo  = new Photo;

//$user = User::find_by_id($_GET['id']);
if (isset($_POST['create'])) {
    if ($user) {
        // $user->id               = $_GET['id'];    
        $user->username     = $_POST['username'];
        $user->password     = $_POST['password'];/////--------------------- with cleaning!
        $user->first_name   = $_POST['first_name'];
        $user->last_name    = $_POST['last_name'];
        $user->user_role    = $_POST['role'];
        $user->set_file($_FILES['user_image']);
        $user->upload_image();
        $user->save();
        redirect("users.php");
        $session->message("User {$user->username} Created!");
    }
}

?>


    <!-- Navigation -->

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

    <!-- Brand and toggle get grouped for better mobile display -->

    <?php include("includes/top_nav.php");  ?>

    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

    <?php include("includes/side_nav.php"); ?>
    
    <!-- /.navbar-collapse -->

</nav>

<div id="page-wrapper">

	<div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">

                <h1 class="page-header">New user<small> | Work in Progress</small></h1>

                <form action="" method="post" enctype="multipart/form-data">
               		
                    <div class="col-md-6 col-md-offset-3">

                        <div class="form-group">
                            <a class="user_thumb" href="#"><img src="<?php echo $user->user_image_path(); ?>" alt=""></a>
                        </div>

                        <div class="form-group">
                            <input class="input" type="file" name="user_image">
                        </div>

                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" name="username" class="form-control" label="">
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control" label="">
                        </div>

                        <div class="form-group">
                            <label for="first_name">First Name</label>
                            <input type="text" name="first_name" class="form-control" label="">
                        </div>

                        <div class="form-group">
                            <label for="last_name">Last Name</label>
                            <input type="text" name="last_name" class="form-control" label="">
                        </div>

                        <div class="form-group">
                            <label for="role">Role</label>
                            <select class="form-control" name="role" id="">
                                <option value="">select</option>
                                <option value="admin">administrator</option>
                                <option value="contributor">contributor</option>
                            </select>
                        </div>

                        
                         <input class="btn btn-info pull-right" type="submit" name="create">



                        <!-- <div class="form-group">
                            <label for="first_name">First Name</label>
                            <textarea  class="form-control" id="editor" name="description" col="30" rows="10" ><?php echo $user->description; ?></textarea>                            
                        </div> -->

                    </div><!-- /.col-md-8 -->

                </form>
                    <!-- ------------------------------------------------------------------------------------ -->

                    

                <!-- ------------------------------------------------------------------------------------ -->                 

            </div><!-- /.col-md-12 -->
    	</div> <!-- /.row -->
	</div><!-- /.container-fluid -->              
</div> <!-- /#page-wrapper -->
            


   

<?php include("includes/footer.php"); ?>