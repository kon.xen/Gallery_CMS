<?php 

include("includes/header.php"); 

include("includes/check_session.php"); 

$photos     = Photo::find_all();

$category   = new Category;



                          

?>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

    <!-- Brand and toggle get grouped for better mobile display -->

    <?php include("includes/top_nav.php");  ?>

    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

    <?php include("includes/side_nav.php"); ?>
    
    <!-- /.navbar-collapse -->

</nav>

<div id="page-wrapper">

	<div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">

                <h1 class="page-header">Uploaded Photos</h1>
                 <h3 class="bg-danger"><?php echo $message ?></h3>
            
           		<div class="col-md-12">

                 <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Photo</th>
                                <th>Id</th>
                                <th>File name</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Size</th>
                                <th>Comments</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach ($photos as $photo) : ?>                             
                            <tr>
                                <td>
                                    <img class="photo_thumb" src="<?php echo $photo->picture_path(); ?>" alt="<?php echo $photo->alternative_text; ?>">
                                   
                                    <div class="action_links pillow ">
                                        <a class="delete_link btn-sm btn-danger" href="delete_photo.php?id=<?php echo $photo->id; ?>">Delete</a>
                                        <a class="btn-sm btn-warning" href="edit_photo.php?id=<?php echo $photo->id; ?>&catid=<?php echo $photo->category_id?>">Edit</a>
                                        <a class="btn-sm btn-info"  href="../photo.php?id=<?php echo $photo->id; ?>">View</a>
                                    </div>
                                </td>

                                <td><?php echo $photo->id; ?></td>
                                <td><?php echo $photo->filename; ?></td>
                                <td><?php echo $photo->title;    ?></td>
                                <td>
                                    <?php                                                                             
                                        $cat   = $category->find_category($photo->category_id);
                                    ?>
                                        
                                </td>
                                <td><?php echo $photo->size;     ?></td>
                                <td>
                                    <a href="photo_comment.php?id=<?php echo $photo->id ?>">
                                        <?php echo count(Comment::find_the_comments($photo->id)); ?>
                                    </a>
                                </td>
                            </tr>
                            <?php endforeach; ?>

                        </tbody>
                    </table><!-- end of Table -->   

                </div><!-- /.col-md-12 -->

        	</div>
    	</div>
                <!-- /.row -->
	</div>
            <!-- /.container-fluid -->

</div>

    <!-- /#page-wrapper -->

<?php include("includes/footer.php"); ?>