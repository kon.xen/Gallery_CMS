<?php include("includes/init.php"); ?>
<?php if (!$session->is_signed_in()){ redirect("login.php"); } ?>

<?php 

if(empty($_GET['id'])){
	
	redirect("comments.php");

} else {

	$comment = Comment::find_by_id($_GET['id']);

	if($comment){

		$comment->delete();
		$session->message("Comment for photo {$comment->photo_id} deleted");
	}

	header("Location:comments.php");
}//


 ?>