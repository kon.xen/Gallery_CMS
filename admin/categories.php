<?php 

include("includes/header.php"); 

include("includes/check_session.php");

$category   = new Category;
$categories = category::find_all();

//Create
if (isset($_POST['add'])) {

    $category->cat_title = $_POST['cat_title'];

    $category->save();
       
    redirect("categories.php");
}

?>


<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

    <!-- Brand and toggle get grouped for better mobile display -->

    <?php include("includes/top_nav.php");  ?>

    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

    <?php include("includes/side_nav.php"); ?>
    
    <!-- /.navbar-collapse -->

</nav>

<div id="page-wrapper">

	<div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            
            <h1 class="page-header">Categories<small> | Work in Progress</small></h1>

            <div class="col-xs-6">

                <!-- Add Category form -->
                <form action="" method="post">
                    <div class="form-group">
                        <label for="cat_title">Add Category</label>
                        <input class="form-control" type="text" name="cat_title">
                    </div>
                
                    <div class="form-group">
                        <input class="btn btn-primary" type="submit" name="add" value="Add Category">
                    </div>
                </form><!-- / Add Category form --> 
            
            </div>


                    <div class="col-xs-6"><!-- Category list -->
                    
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Category title</th>
                                    <th></th>
                                    <th></th> 
                                </tr>
                                
                            </thead>

                            <tbody>
                                <?php foreach ($categories as $cat) : ?>
                                <tr>
                                    <td><?php echo $cat->id; ?></td>
                                    <td><?php echo $cat->cat_title; ?></td>
                                    <td>
                                        <div class="action_links">
                                            <a class="delete_link btn btn-danger" href="delete_category.php?id=<?php echo $cat->id; ?>">Delete</a>
                                            <a class="btn btn-warning"            href="update_category.php?id=<?php echo $cat->id; ?>">Edit</a>
                                        </div>
                                    </td>
                                    
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                         </table>

                    </div><!-- / Category list -->       

                </div><!-- / Column -->

            </div><!-- / Row -->                


        	</div>
    	</div>
                <!-- /.row -->
	</div>
            <!-- /.container-fluid -->

</div>

    <!-- /#page-wrapper -->

<?php include("includes/footer.php"); ?>