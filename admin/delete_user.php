<?php include("includes/init.php"); ?>
<?php if (!$session->is_signed_in()){ redirect("login.php"); } ?>

<?php 

if(empty($_GET['id'])){
	
	redirect("users.php");

} else {

	$user = User::find_by_id($_GET['id']);
	
	if($user){

		$user->delete_user();
	 	$session->message("User {$user->username} & image is Deleted!");
		
	 	redirect("users.php");
	}
	
	header("Location:users.php");
}

?>