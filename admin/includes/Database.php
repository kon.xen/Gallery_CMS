<?php 

require_once("db_config.php");


class Database
{

// !***    Options    ***!
	
	public $connection;

// !***    Constructors    ***!
	
	function __construct()
	{
		$this->open_db_connection();
	}

// !***    Methods    ***!
	
	public function open_db_connection()
	{

		$this->connection = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME); // this is an object because of the "new" keyword containing propertiesd and methods...

		if ($this->connection->connect_errno) { // errno is a property of the  mysqli class !

			die("Database connection failed badly" . $this->connection->connect_error);
		}

	}//

	public function query($sql)
	{

		$result = $this->connection->query($sql);
		$this->confirm_query($result);
		
		return $result;

	}//


	private function confirm_query($result)
	{

		if (!$result) {
			
			die("Query Failed". $this->connection->error);
		}

	}//

	public function escape_string($string)
	{

		$escaped_string = $this->connection->real_escape_string($string);
		
		return $escaped_string;

	}//

	public function the_insert_id()
	{
		
		return $this->connection->insert_id; 
	
	}//

} // end of class

$database = new Database();
