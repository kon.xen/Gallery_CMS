<?php 

class Db_object
{

	// !***    Options    ***!

	public $errors 					= array ();
		
	// upload erros array TBC!! also in the photo class! one should be deleted at the end...
	
	public $upload_errors_array 	= array (
		   UPLOAD_ERR_OK			=> "There is no error :)",
		   UPLOAD_ERR_INI_SIZE		=> "The uploaded file exceeds the upload_max_filesize directive in php.ini !",
		   UPLOAD_ERR_FORM_SIZE		=> "The uploaded file exceeds the MAX_FILE_directive as specified in php.ini !",
		   UPLOAD_ERR_PARTIAL		=> "The uploaded file was only partialy uploaded !",
		   UPLOAD_ERR_NO_FILE		=> "No file was uploaded !",
		   UPLOAD_ERR_NO_TMP_DIR	=> "Missing a temporary folder !",
		   UPLOAD_ERR_CANT_WRITE	=> "Failed to write file to disk !",
		   UPLOAD_ERR_EXTENSION 	=> "A PHP extension stopped the file upload !",
		   );


	// !***    Methods         ***!


	public static function find_by_query($sql)
	{

		global $database;

		$result_set = $database->query($sql);
		$the_object_array = array();

		while ($row = mysqli_fetch_array($result_set)) {
			$the_object_array[] = static::instantiation($row);
		}

		return $the_object_array;	
	}//



	public static function find_all()
	{

		$table  = static::$db_table;

		return static::find_by_query("SELECT * FROM $table");
	
	}//



	public static function find_by_id($id)
	{

		$table  = static::$db_table;

		$result_array = static::find_by_query("SELECT * FROM $table WHERE id = $id LIMIT 1");
		
		return !empty($result_array) ? array_shift($result_array) : false;	//ternary operator ?: check online
					
	}//



	public static function instantiation($the_record)
	{	

		// creates classs properties in the instance $the_object

		$calling_class = get_called_class();
		$the_object = new $calling_class;

		foreach ($the_record as $attribute => $value) {

			if  ($the_object->has_the_attribute($attribute)){
				$the_object->$attribute = $value;
			}
		}

        return $the_object;
	}//	



	private function has_the_attribute($attribute)
	{

		$object_properties = get_object_vars($this);
		// php function that gets the properties of the given object
		
		return array_key_exists($attribute, $object_properties);	
		// php function that Checks if the given key or index exists in the array
	}//



	public function properties()
	{
		//return get_object_vars($this);
		$properties = array();
		foreach (static::$db_table_fields as $db_field) {
			if (property_exists($this, $db_field)) {
				$properties[$db_field] = $this->$db_field;
			}
		}
		return $properties;
	}//



	protected function clean_properties(){
		global $database;

		$clean_properties = array();

		foreach ($this->properties() as $key => $value) {
			$clean_properties[$key] = $database->escape_string($value);
		}

		return $clean_properties;
	}//



	public function save(){
	
		return isset($this->id) ? $this->update() : $this->create();
	}// end of Save



	public function create(){

		global $database;
		$properties 	 = $this->clean_properties();
		$table 	 		 = static::$db_table;
		$tidy_properties = implode(",", array_keys($properties));
		$tidy_values 	 = implode("','", array_values($properties));

		$sql = "INSERT INTO $table ($tidy_properties) VALUES ('{$tidy_values}')";
		//if it doesn't work try '{}'
		
		if($database->query($sql)){

			$this->id = $database->the_insert_id();

			return true;

		} else {

			return false;
		}	

	}// end of create



	public function update()
	{

		global $database;
		$properties 	  = $this->clean_properties();
		$properties_pairs = array();
		$table 			  = static::$db_table;
		
		
		foreach ($properties as $key => $value) {
			$properties_pairs[] = "{$key}='{$value}'";
		}

		$sql = "UPDATE $table SET " . implode(",", $properties_pairs). " WHERE id =". $database->escape_string($this->id); 
		
		$database->query($sql);

		return (mysqli_affected_rows($database->connection) == 1) ? true : false;

	}// end of update



	public function delete()
	{
		global $database;
		$table = static::$db_table;

		$sql   = "DELETE FROM $table WHERE id = {$database->escape_string($this->id)} LIMIT 1"; 
				
		$database->query($sql);

		return (mysqli_affected_rows($database->connection) == 1) ? true : false;

	}// end of delete



	public static function count_all()
	{
		
		global $database;

		$sql = "SELECT COUNT(*) FROM " . static::$db_table;
		$result_set = $database->query($sql);
		$row = mysqli_fetch_array($result_set);

		return array_shift($row);

	}//end  of count all

}// end of class Db_object
