<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Admin
                <small>Control panel</small>
            </h1>

            <div class="row">
                                               
                 <!-- Photos box -->   
                 <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-photo fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo Photo::count_all(); ?></div>
                                    <div>Photos</div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
               
            </div><!--First Row-->

            <!-- <div class="row">
                <div id="piechart" style="width: 900px; height: 500px;"></div>
            </div> --><!--Second Row-->

        </div><!-- /.Column-->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->