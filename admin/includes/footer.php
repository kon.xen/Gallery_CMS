  </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/10.0.1/classic/ckeditor.js"></script>
  
    <script src="js/scripts.js"></script>
    <script src="js/dropzone.js"></script>




	<!-- Google Chart Script -->
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Site content'],
          ['Views',     <?php echo $session->count; ?>],
          ['Photos',    <?php echo Photo::count_all(); ?>],
          ['Users',     <?php echo User::count_all(); ?>],
          ['Comments',  <?php echo Comment::count_all(); ?>],
          
        ]);

        var options = {
          pieSliceText:     'label',
          legend:           'none',
          backgroundColor:  'transparent',
          is3D:             'true',
          title:            'Content'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>

</body>

</html>
