<?php 

function autoLoader($class){
	$class = strtolower($class);
	$the_Path = "includes/{$class}";


	if(is_file($the_Path) && !class_exists($class)){
		include $the_Path2;
	}
	
}

spl_autoload_register('autoLoader');


function redirect($location){
	header("Location: {$location}");
}

 ?>