<?php 

class User extends Db_object 
{

// !***    Properties         ***!
	protected static $db_table = "users";
	protected static $db_table_fields = array(
		'username', 
		'password',
		'first_name', 
		'last_name', 
		'user_image', 
		'user_role'
	);

	public $id;
	public $username;
	public $first_name;
	public $last_name;
	public $user_image;
	public $user_role;
	public $upload_dir 		  = "images";
	public $image_placeholder = "./images/user-icon-placeholder.png";

	
	

// !***    Methods    ***!

	public function ajax_save_user_image($user_image, $user_id)
	{
		global $database;
		$user_image 		= $database->escape_string($user_image);
		$user_id 			= $database->escape_string($user_id);

		$this->user_image  	= $user_image;
		$this->id			= $user_id;

		$sql  				= "UPDATE ". self::$db_table . " SET user_image = '{$this->user_image}' ";
		$sql 			   .= " WHERE id = {$this->id}";

		$update_image 		= $database->query($sql);

		echo $this->user_image_path();
		
		$this->save();

	}



	public static function verify_user($username, $password)
	{
		global $database;

		$username = $database->escape_string($username);
		$password = $database->escape_string($password);

		$sql = "SELECT * FROM ". self::$db_table." WHERE username = '{$username}' AND password = '{$password}' LIMIT 1";

		$result_array = self::find_by_query($sql);
		
		return !empty($result_array) ? array_shift($result_array) : false;
	}//



	public function user_image_path()
	{
		return empty($this->user_image) ? $this->image_placeholder : $this->upload_dir . DS . $this->user_image;
	}//



	public function delete_user()
	{

		if ($this->delete()) {

			$target_path = $this->upload_dir . DS . $this->user_image;
			return unlink($target_path) ? true : false;

		} else {

			return false;	
		}			

	}//


		
	public function set_file($file) {
		
		if (empty($file) || !$file || !is_array($file)) {

			$this->errors[] = "There was no file to upload !";
			return false;

		} elseif ($file['error'] !=0) {

			$this->errors[] = $this->upload_errors_array[$file['error']];
			return false;
	
		} else {

		$this->user_image = basename($file['name']);
		$this->tmp_path   = $file['tmp_name'];

		}
		
	}//



	public function upload_image()
	{
		if (!empty($this->errors)) {
			return false;
		}

		if (empty($this->user_image) || empty($this->tmp_path)) {
			$this->errors[] = "the file was not available";
			return false;
		}

		$target_path = SITE_ROOT . DS . 'admin' . DS . $this->upload_dir . DS . $this->user_image;
		
		if (file_exists($target_path)) {
			$this->errors[] = "File {$this->user_image} already exists !";
			return false;
		}

		if (move_uploaded_file($this->tmp_path, $target_path)) {
			
				unset($this->tmp_path);
				return true;

		} else {

			$this->errors[] = "Not allowed to write on disk. Please check permissions !";
			return false;
		}
	}//

	public function role()
	{
			
		if ($this->user_role == 'admin') {

			return true;

		}
	}// 

}// end of class USER
