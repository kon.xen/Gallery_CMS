<?php 

defined("DS") ? null : define('DS', DIRECTORY_SEPARATOR);
defined("SITE_ROOT") ? null : define('SITE_ROOT', DS . 'opt'. DS . 'lampp'. DS .'htdocs'. DS . 'OOP'. 
	DS .'galleryV2');
defined("INCLUDES_PATH") ? null : define('INCLUDES_PATH', SITE_ROOT . DS . 'admin'. DS . 'includes');

require_once (INCLUDES_PATH . DS . "db_config.php"	); 
require_once (INCLUDES_PATH . DS . "Database.php"	);
require_once (INCLUDES_PATH . DS . "Session.php"	);
require_once (INCLUDES_PATH . DS . "Db_object.php"	);
require_once (INCLUDES_PATH . DS . "Photo.php"		);
require_once (INCLUDES_PATH . DS . "Category.php"	);
require_once (INCLUDES_PATH . DS . "User.php"		);
require_once (INCLUDES_PATH . DS . "Paginate.php"	);
require_once (INCLUDES_PATH . DS . "Comment.php"	);
require_once (INCLUDES_PATH . DS . "functions.php"	);
