<div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav side-nav">
        
        <li>
            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
        </li>
        
        <li>
            <a href="upload.php"><i class="fa fa-fw fa-table"></i>Upload</a>
        </li>

        <?php

          if ($loged_user->role()){

            echo "
            <li>
                <a href='photos.php'><i class='fa fa-fw fa-table'></i>Photos</a>
            </li> 
            <li>
                <a href='comments.php'><i class='fa fa-fw fa-edit'></i>Comments</a>
            </li>
            <li>
                <a href='categories.php'><i class='fa fa-fw fa-edit'></i>Categories</a>
            </li>
            <li>
                <a href='users.php'><i class='fa fa-fw fa-edit'></i>Users</a>
            </li>";
            }
        ?>        

    </ul>
</div>