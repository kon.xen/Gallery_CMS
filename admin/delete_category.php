<?php

include("includes/init.php"); 

if (!$session->is_signed_in()) {

	redirect("login.php");

	}


if (empty($_GET['id'])) {
	
	redirect("categories.php");

} else {

	$category = Category::find_by_id($_GET['id']);

	if ($category) {

		$category->delete();
		$session->message("Photo {$category->title} deleted");
	}

	header("Location:categories.php");
}
