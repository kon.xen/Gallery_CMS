<?php

include("includes/header.php"); 

if (!$session->is_signed_in()) {
    redirect("login.php"); 
} 

$message ="";

$categories = Category::find_all();

if (isset($_FILES['file'])) {

    $photo = new Photo();
    $photo->title = $_POST['title'];
    $photo->category_id = $_POST['cat_id'];
    $photo->set_file($_FILES['file']);

    if ($photo->save()) {

        $message = "Photo uploaded succesfully";

    } else {

        $message = join("<br>", $photo->errors);
    }
   
}

?>

    <!-- Navigation -->

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

    <!-- Brand and toggle get grouped for better mobile display -->

    <?php include("includes/top_nav.php");  ?>

    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

    <?php include("includes/side_nav.php"); ?>
    
    <!-- /.navbar-collapse -->

</nav>

<div id="page-wrapper">

	<div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Photo Upload</h1>
                <div class="row">
                    <div class="col-md-6">
                        <?php echo $message; ?>

                        <!-- UPLOAD FORM -->
                        <form action="upload.php" method="post" enctype="multipart/form-data">
                            
                            <div class="form-group">
                                <label for="name">Picture Name</label>
                                <input type="text" name="title" class="form-control" value="Name">
                            </div>

                            <div class="form-group">
                                <input class="input" type="file" name="file">
                            </div>

                            <div class="form-group">
                                <label for="cat_id">Category</label>
                                <select class="form-control" name="cat_id" id="">
                                    <?php foreach ($categories as $category) : ?>
                                        <option value="<?php echo $category->id; ?>"><?php echo $category->cat_title; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <input class="btn btn-info" type="submit" name="submit">

                        </form><!-- Upload form -->
                    </div><!-- Column -->
                </div><!-- ens of row-->
                <div class="row">
                    <div class="col-lg-12">
                        <form class="dropzone" action="upload.php"></form>
                    </div>   
                </div>
                     		
        	</div>
    	</div><!-- /.row -->

	</div><!-- /.container-fluid -->

</div><!-- /#page-wrapper -->

<?php include("includes/footer.php"); ?>