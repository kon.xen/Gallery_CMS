<?php 

include("includes/header.php"); 
include("includes/pagination_data.php"); 
include("includes/display_photos.php"); 
include("includes/carousel_photos.php"); 

?>

<!-- the gallery -->
<div class="container">

    <div class="row">

    <div class="col-sm"></div>

        <!-- Blog Entries Column -->
        <div class="col-md">

            <!-- Carousel -->
            <div class="row">
                <div class="col-sm-3"></div>

                <div class="col-md-6">
                    <div class="container-fluid">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                      <!-- Indicators -->
                      <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                        <li data-target="#myCarousel" data-slide-to="4"></li>
                      </ol>

                      <!-- Wrapper for slides -->
                      <div class="carousel-inner carouphoto">
                        <div class="item active">
                          <img src="admin/images/<?php echo $image[1]; ?>" alt="<?php echo $alter[1]; ?>">
                        </div>

                        <div class="item carouphoto">
                          <img src="admin/images/<?php echo $image[2]; ?>" alt="<?php echo $alter[2]; ?>">
                        </div>

                        <div class="item carouphoto">
                          <img src="admin/images/<?php echo $image[3]; ?>" alt="<?php echo $alter[3]; ?>">
                        </div>

                        <div class="item carouphoto">
                          <img src="admin/images/<?php echo $image[4]; ?>" alt="<?php echo $alter[4]; ?>">
                        </div>

                        <div class="item carouphoto">
                          <img src="admin/images/<?php echo $image[5]; ?>" alt="<?php echo $alter[5]; ?>">
                        </div>
                      </div>

                      <!-- Left and right controls -->
                      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                </div>

                    </div>
                <div class="col-sm-3"></div>
            </div><!-- carousel row -->
          <hr>

            <div class="row pd-20 ">
                <?php foreach ($photos as $photo):?>
                  <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                      <img class="home_page_photo" src="admin/<?php echo $photo->picture_path(); ?>" alt="gallery image thumbnail">
                      <div class="caption">
                        <h3><?php echo $photo->title; ?></h3>
                        <!-- p><?php //echo $photo->caption; ?></p> -->
                        <p><a href="photo.php?id=<?php echo $photo->id; ?>" class="btn btn-primary" role="button">view</a></p>
                      </div>
                    </div>
                  </div>
                <?php endforeach; ?>
             </div>

           <!--  <div class="thumbnails row"> OLD THUMBBNAILS VIEW

                <?php //foreach ($photos as $photo):?>
                    <div class="col-xs-6 col-md-3">
                        <a class="thumbnail" href="photo.php?id=<?php // echo $photo->id; ?>">
                            <img class="home_page_photo img-responsive" src="admin/<?php // echo $photo->picture_path(); ?>" alt="">
                        </a>
                    </div>
                <?php// endforeach; ?> 

            </div>
 -->
        
            <div class='row'>
                    <ul class="pager">
                        <?php

                            include("includes/pager.php");                             
                        ?>                     
                    </ul>
                </div>

            
                            
        </div><!-- /.col-md -->

    <div class="col-sm"></div>

    </div><!-- /.row -->    

    <?php include("includes/footer.php"); ?>
