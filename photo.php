<?php 

include("includes/header.php"); 
include("includes/photo_data.php"); 

?>


 <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-12">

                <!-- Blog Post -->

                <!-- Title -->
                <h1><?php echo $photo->title ?></h1>
                              

                <hr>

                <!-- Date/Time -->
                <!-- <p><span class="glyphicon glyphicon-time"></span> Posted on August 24, 2013 at 9:00 PM</p>

                <hr> -->

                <!-- Image -->
                
                <img class="img-responsive" src="admin/<?php echo $photo->picture_path(); ?>" alt="posted photo">
               

                <hr>

                <!-- Image description -->
                <p class="lead"><?php echo $photo->caption ?></p>
                <p> Category: <?php $category->find_category($photo->category_id);?></p>
                <p><?php echo $photo->description ?></p>
                                              
                <hr>

                <!-- Blog Comments -->

                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    
                    <form role="form" method="post">
                        
                        <div class="form-group">
                            <label for="author">Author</label>
                            <input class="form-control" type="text" name="author">
                        </div>

                        <div class="form-group">
                            <textarea name="body" class="form-control" rows="3"></textarea>
                        </div>

                        <button class="btn btn-primary" type="submit" name='submit'>Submit</button>

                    </form>
                </div> <!-- Well -->


                <hr>
                <!-- Posted Comments -->                              
                <?php foreach ($comment as $comment) : ?>                                            
                <div class="media">
                    
                    <a class="pull-left" href="#">
                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                    </a>

                    <div class="media-body">
                        <h4 class="media-heading">Start Bootstrap
                            <small>August 25, 2014 at 9:30 PM</small>
                        </h4>
                        <?php echo $comment->body;?>                        
                    </div> 

                </div>
                <?php endforeach; ?>
                    
           </div><!-- /.col-lg-8 -->
            
        </div><!-- /.row -->
  
        
    <?php include("includes/footer.php"); ?>