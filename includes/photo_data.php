<?php 

if (empty($_GET['id'])) {
    
    redirect("index.php");
}
$category = new Category;
$photo    = Photo::find_by_id($_GET['id']);
$path     = "/admin/".$photo->picture_path();

if (isset($_POST['submit'])) {

    $author = trim($_POST['author']);
    $body   = trim($_POST['body']);

    $new_comment = Comment::create_comment($photo->id, $author, $body);

    if ($new_comment && $new_comment->save()) {
        
        redirect("photo.php?id={$photo->id}");

    } else {

        $message = "unable to save !"; 
    }

 } else {

    $author = "";
    $body   = "";

}//



$comment = Comment::find_the_comments($photo->id);
