    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Home</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    
                    <li>
                        <a href="about.php">About</a>
                    </li>

                    <!-- DropDown -->  
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categories <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php foreach ($categories as $category) : ?>
                                <li role="separator" class="divider"></li>   
                                <li><a href="category_view.php?catid=<?php echo $category->id; ?>"><?php echo $category->cat_title;?></a></li>
                                
                            <?php endforeach; ?>
                        </ul>
                    </li>
           
                    <li>
                        <?php
                            if ($session->is_signed_in()) {
                                echo "<a href='admin/index.php'>User</a>";
                            }
                        ?>    
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
            
        </div><!-- /.container -->
    </nav>