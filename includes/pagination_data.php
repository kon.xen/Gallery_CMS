<?php 

// pagination parameters.
$page               = !empty($_GET['page']) ? (int)$_GET['page'] : 1;
$items_per_page     = 3;
$items_total_count  = Photo::count_all();
