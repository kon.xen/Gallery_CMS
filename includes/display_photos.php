<?php 

// find all the photos to display.

$photos   = Photo::find_all();

$paginate = new Paginate($page, $items_per_page, $items_total_count);

$sql  	  = "SELECT * FROM photos ";
$sql 	 .= "LIMIT {$items_per_page} ";
$sql 	 .= "OFFSET {$paginate->offset()}";

$photos   = Photo::find_by_query($sql);
