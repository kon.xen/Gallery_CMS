<?php 

if ($paginate->page_total() > 1) {

    if (!$paginate->has_previous()) {

        echo "<li class='previous disabled'><a href=''>Previous</li></a>";
    } else {
        
        echo "<li class='previous'><a href='index.php?page={$paginate->previous()}'</a>
        Previous</li>";
    }//


    if (!$paginate->has_next()) {

        echo "<li class='next disabled'><a href=''>Next</li></a>";
    } else {

        echo "<li class='next'><a href='index.php?page={$paginate->next()}'</a>
        Next</li>";
    }

    for ($i=1; $i <= $paginate->page_total(); $i++) {

        if ($i == $paginate->current_page) {

            echo "<li class='active'><a href='index.php?page={$i}'>{$i}</a></li>";
        } else {

            echo "<li class=''><a href='index.php?page={$i}'>{$i}</a></li>";
        }

    }// for 
}//IF 
