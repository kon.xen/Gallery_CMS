<?php 

include("includes/header.php"); 
include("includes/pagination_data.php"); 

$category = $_GET['catid'];

$paginate = new Paginate($page, $items_per_page, $items_total_count);

$sql      = "SELECT * FROM photos WHERE category_id = $category ";

$photos   = Photo::find_by_query($sql);


?>

<!-- the gallery -->
<div class="container">

    <div class="row">

    <div class="col-sm"></div>

        <!-- Blog Entries Column -->
        <div class="col-md">          

            <div class="row pd-20 ">
                <?php foreach ($photos as $photo):?>
                  <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                      <img class="home_page_photo" src="admin/<?php echo $photo->picture_path(); ?>" alt="gallery image thumbnail">
                      <div class="caption">
                        <h3><?php echo $photo->title; ?></h3>
                        <!-- p><?php //echo $photo->caption; ?></p> -->
                        <p><a href="photo.php?id=<?php echo $photo->id; ?>" class="btn btn-primary" role="button">view</a></p>
                      </div>
                    </div>
                  </div>
                <?php endforeach; ?>
             </div>

           <!--  <div class="thumbnails row"> OLD THUMBBNAILS VIEW

                <?php //foreach ($photos as $photo):?>
                    <div class="col-xs-6 col-md-3">
                        <a class="thumbnail" href="photo.php?id=<?php // echo $photo->id; ?>">
                            <img class="home_page_photo img-responsive" src="admin/<?php // echo $photo->picture_path(); ?>" alt="">
                        </a>
                    </div>
                <?php// endforeach; ?> 

            </div>
 -->
        
            

            
                            
        </div><!-- /.col-md -->

    <div class="col-sm"></div>

    </div><!-- /.row -->    

    <?php include("includes/footer.php"); ?>
